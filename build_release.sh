# $1 is the container's tag or version (e.g. 1.0.15)
# $2 is the name of the repo being built in ./apps (e.g. www, api)

VER="$1"
REGISTRY="registry.gitlab.com/mariomejia/trackerapi"


docker build -t $REGISTRY:$VER .
docker push $REGISTRY:$VER

