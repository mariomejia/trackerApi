# Shortcut script for running docker-compose with staging configuration
echo "✨ Pull, stopping and running api..."
# docker-compose -f docker-compose.yml -f docker-compose.staging.yml pull "$@"
docker-compose.yml  pull api
docker stop api
docker-compose.yml  up -d api
